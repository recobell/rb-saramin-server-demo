DROP TABLE IF EXISTS recruit;
CREATE TABLE recruit (
rec_idx int NOT NULL DEFAULT '0' PRIMARY KEY, /* COMMENT '채용공고번호'--*/
--@career_cd int DEFAULT '0' , /* COMMENT '경력코드' */
--@career_min int DEFAULT NULL , /* COMMENT '경력하한' */
--@career_max int DEFAULT NULL , /* COMMENT '경력상한' */
sex_lim varchar(8) NOT NULL DEFAULT 'ignore' , /* COMMENT '성별제한'*/
--@education_cd int DEFAULT '0' , /* COMMENT '최종학력코드' */
--@age_min int NOT NULL DEFAULT '0' , /* COMMENT '나이제한하한'*/
--@age_max int NOT NULL DEFAULT '0' , /* COMMENT '나이제한상한'*/
--@closing_dt datetime DEFAULT NULL , /* COMMENT '채용마감일시'--*/
--@status int NOT NULL DEFAULT '0', /* COMMENT '노출상태'*/
company_nm varchar(200) DEFAULT NULL, /* COMMENT '기업명'*/
title varchar(200) DEFAULT NULL /* COMMENT '공고제목'*/
) default character set utf8 collate utf8_general_ci; /* COMMENT='채용공고' */

DROP TABLE IF EXISTS saraminrec;
CREATE TABLE saraminrec (
mem_idx int NOT NULL,
rec_idx int NOT NULL,
recmd_rank int NOT NULL
) default character set utf8 collate utf8_general_ci;

DROP TABLE IF EXISTS saraminasc;
CREATE TABLE saraminasc (
target_rec_idx int NOT NULL,
cross_rec_idx int NOT NULL,
recmd_rank int NOT NULL
) default character set utf8 collate utf8_general_ci;