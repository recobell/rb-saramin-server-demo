package com.recobell.saramin.server.demo;

import java.nio.charset.Charset;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.filter.CharacterEncodingFilter;

import javax.servlet.Filter;

/**
 * 
 * @author yeonjejun
 * @since 2016. 3. 4.
 */
@Controller
public class SaraminServerDemoController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    JdbcTemplate jdbcTemplate;

    public static final String PSN_RESULT_QUERY = "SELECT A.mem_idx, B.rec_idx, B.company_nm, B.title FROM (SELECT * FROM saraminrec WHERE mem_idx= ? ) A INNER JOIN recruit B ON A.rec_idx = B.rec_idx ORDER BY A.recmd_rank ASC;";
    public static final String ASC_RESULT_QUERY = "select 0, a.cross_rec_idx, b.company_nm, b.title from saraminasc a inner join recruit b on a.cross_rec_idx = b.rec_idx where a.target_rec_idx = ? order by a.recmd_rank asc;";

    public static final String GET_REC = "select 0, rec_idx, company_nm, title from recruit where rec_idx = ? limit 1;";

    public static final String GET_RANDOM_MEMIDX = "select mem_idx from mem_list order by rand() limit 1;";
    public static final String GET_RANDOM_RECIDX = "select target_rec_idx rec_idx from saraminasc order by rand() limit 1;";

    @RequestMapping("/")
    public String index(Model model, @RequestParam(value = "memIdx", required = false) String memIdx) {
        model.addAttribute("recList", getPsnRec(memIdx));
        return "index";
    }

    @RequestMapping(value = "/psn", method = RequestMethod.GET)
    public String psn(Model model, @RequestParam(value = "memIdx", required = false) String memIdx) {
        model.addAttribute("recList", getPsnRec(memIdx));
        return "index";
    }

    @RequestMapping(value = "/asc", method = RequestMethod.GET)
    public String asc(Model model, @RequestParam(value = "recIdx", required = false) String recIdx) {

        if (recIdx == null || recIdx.isEmpty()) {
            int tmp = jdbcTemplate.queryForObject(GET_RANDOM_RECIDX, Integer.class);
            recIdx = String.valueOf(tmp);
        }
        model.addAttribute("recruit", getRecruit(recIdx));
        model.addAttribute("recList", getAscRec(recIdx));
        return "asc";
    }

    private RecruitData getRecruit(String recIdx) {
        return (RecruitData) jdbcTemplate.query(GET_REC, new RecruitDataRowMapper(), recIdx).get(0);
    }

    private ArrayList<RecruitData> getPsnRec(String memIdx) {
        ArrayList<RecruitData> recruitList = new ArrayList<RecruitData>();
        if (memIdx == null || memIdx.isEmpty()) {
            int tmp = jdbcTemplate.queryForObject(GET_RANDOM_MEMIDX, Integer.class);
            memIdx = String.valueOf(tmp);
        }
        recruitList = (ArrayList<RecruitData>) jdbcTemplate.query(PSN_RESULT_QUERY, new RecruitDataRowMapper(), memIdx);
        return recruitList;
    }

    private ArrayList<RecruitData> getAscRec(String recIdx) {
        ArrayList<RecruitData> recruitList = new ArrayList<RecruitData>();

        recruitList = (ArrayList<RecruitData>) jdbcTemplate.query(ASC_RESULT_QUERY, new RecruitDataRowMapper(), recIdx);

        return recruitList;
    }

    @Bean
    public HttpMessageConverter<String> responseBodyConverter() {
        return new StringHttpMessageConverter(Charset.forName("UTF-8"));
    }

    @Bean
    public Filter characterEncodingFilter() {
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        characterEncodingFilter.setForceEncoding(true);
        return characterEncodingFilter;
    }

}
