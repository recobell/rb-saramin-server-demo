package com.recobell.saramin.server.demo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.zip.GZIPInputStream;

import javax.annotation.PostConstruct;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.filter.CharacterEncodingFilter;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;

@SpringBootApplication
@EnableScheduling
@EnableAutoConfiguration
public class SaraminServerDemoApplication {

    @Value("${recobell.saramindemo.bucket}")
    private String s3Bucket;

    @Value("${recobell.saramindemo.psnSourceFileRec}")
    private String psnSourceFileRec;

    @Value("${recobell.saramindemo.ascSourceFileRec}")
    private String ascSourceFileRec;

    @Value("${recobell.saramindemo.sourceFileDat}")
    private String sourceFileDat;

    public static final String INSERT_PSN = "INSERT INTO saraminrec (mem_idx, rec_idx, recmd_rank) VALUES (?, ?, ?)";
    public static final String INDEX_PSN = "CREATE INDEX rec_mem_idx on saraminrec(mem_idx);";
    public static final String INDEX2_PSN = "CREATE INDEX rec_rec_idx on saraminrec(rec_idx);";

    public static final String INSERT_ASC = "INSERT INTO saraminasc (target_rec_idx, cross_rec_idx, recmd_rank) VALUES (?, ?, ?)";
    public static final String INDEX_ASC = "CREATE INDEX rec_asc_idx_1 on saraminasc(target_rec_idx);";
    public static final String INDEX2_ASC = "CREATE INDEX rec_asc_idx_2 on saraminasc(cross_rec_idx);";

    public static final String INSERT_DAT = "INSERT INTO recruit (rec_idx, sex_lim, company_nm, title) VALUES (?, ?, ?, ?)";

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    AmazonS3 s3;

    private Logger logger = LoggerFactory.getLogger(getClass());

    public static void main(String[] args) {
        SpringApplication.run(SaraminServerDemoApplication.class, args);
    }

    @Scheduled(cron = "0 0 12 * * *")
    public void scheduledUpdate()
        throws IOException
    {
        updateRecmd();
    }

    @PostConstruct
    public void updateRecmd()
        throws IOException
    {
        loadData(psnSourceFileRec);
        loadData(ascSourceFileRec);
        loadData(sourceFileDat);
        refineMemlist();
    }

    private void refineMemlist() {
        logger.info("starts to refine mem list");
        jdbcTemplate.execute("DROP TABLE IF EXISTS mem_list;");
        jdbcTemplate.execute("CREATE TABLE mem_list AS SELECT DISTINCT mem_idx FROM saraminrec;");
        logger.info("finished to refine mem list");
    }

    private void loadData(String sourceFileName)
        throws IOException
    {

        logger.info("data loading...{}", sourceFileName);
        try (S3Object s3Object = s3.getObject(s3Bucket, sourceFileName);
            final InputStream is = s3Object.getObjectContent();
            final GZIPInputStream gis = new GZIPInputStream(is);
            final InputStreamReader isr = new InputStreamReader(gis, "utf8");
            final BufferedReader br = new BufferedReader(isr);
            CSVParser parser = new CSVParser(br, CSVFormat.DEFAULT);)
        {

            String sql = "";
            if (sourceFileName.contains("recruit")) {
                sql = INSERT_DAT;
                jdbcTemplate.execute("TRUNCATE TABLE recruit");
            } else if (sourceFileName.contains("tb_psn_recmd_result")) {
                sql = INSERT_PSN;
                jdbcTemplate.execute("TRUNCATE TABLE saraminrec");
            } else if (sourceFileName.contains("tb_asc_recmd_result")) {
                sql = INSERT_ASC;
                jdbcTemplate.execute("TRUNCATE TABLE saraminasc");
            }

            try (
                Connection con = jdbcTemplate.getDataSource().getConnection();
                PreparedStatement ps = con.prepareStatement(sql);)
            {
                int cnt = 0;

                for (CSVRecord record : parser) {

                    if (record.size() < 4) {
                        ps.setInt(1, Integer.parseInt(record.get(0)));
                        ps.setInt(2, Integer.parseInt(record.get(1)));
                        ps.setInt(3, Integer.parseInt(record.get(2)));
                    } else {
                        ps.setInt(1, Integer.parseInt(record.get(0)));
                        ps.setString(2, record.get(4));
                        ps.setString(3, record.get(10));
                        ps.setString(4, record.get(11));
                    }
                    ps.addBatch();
                    cnt++;

                    if (cnt % 10000 == 0) {
                        logger.info("{} is loading, ... {} lines", sourceFileName, cnt);
                        //logger.info("{}", ps.toString());
                        ps.executeBatch();
                    }
                }

                ps.executeBatch();

            } catch (SQLException e) {
                logger.error("error to connect / insert into h2", e);
            }

        } catch (AmazonServiceException e) {
            // TODO : handling
            throw new IOException();
        }

        if (sourceFileName.contains("recruit")) {} else if (sourceFileName.contains("tb_psn_recmd_result")) {
            jdbcTemplate.execute(INDEX_PSN);
            jdbcTemplate.execute(INDEX2_PSN);
        } else if (sourceFileName.contains("tb_asc_recmd_result")) {
            jdbcTemplate.execute(INDEX_ASC);
            jdbcTemplate.execute(INDEX2_ASC);
        }

        logger.info("data loaded {}", sourceFileName);

    }

}
