package com.recobell.saramin.server.demo;
/**
 * 
 * @author 	yeonjejun
 * @since	2016. 3. 9.
 */
public class RecruitData {
    
    private int memIdx;
    private int recIdx;
    private String companyNm;
    private String title;
    public int getMemIdx() {
        return memIdx;
    }
    public void setMemIdx(int memIdx) {
        this.memIdx = memIdx;
    }
    public int getRecIdx() {
        return recIdx;
    }
    public void setRecIdx(int recIdx) {
        this.recIdx = recIdx;
    }
    public String getCompanyNm() {
        return companyNm;
    }
    public void setCompanyNm(String companyNm) {
        this.companyNm = companyNm;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    
    @Override
    public String toString() {
        return "memIdx : " + memIdx + " / recIdx : " + recIdx + " / companyNm : " + companyNm + " / title : " + title;
    }
    
}
