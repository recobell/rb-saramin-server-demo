package com.recobell.saramin.server.demo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

/**
 * 
 * @author 	yeonjejun
 * @since	2016. 3. 10.
 */
public class RecruitDataRowMapper implements RowMapper<RecruitData> {

    @Override
    public RecruitData mapRow(ResultSet rs, int rowNum)
        throws SQLException
    {
        RecruitData recruitData = new RecruitData();
        recruitData.setMemIdx(rs.getInt(1));
        recruitData.setRecIdx(rs.getInt(2));
        recruitData.setCompanyNm(rs.getString(3));
        recruitData.setTitle(rs.getString(4));
        return recruitData;
    }

}
