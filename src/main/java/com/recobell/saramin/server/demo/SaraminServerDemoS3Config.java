package com.recobell.saramin.server.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

/**
 * 
 * @author 	yeonjejun
 * @since	2016. 3. 4.
 */
@Configuration
public class SaraminServerDemoS3Config {

    @Value("${aws.accessKeyId}")
    private String accessKeyId;
    
    @Value("${aws.secretKey}")
    private String secretKey;
    
    @Value("${recobell.saramindemo.s3.endpoint}")
    private String s3Endpoint;
    
    @Value("${recobell.saramindemo.s3.maxConnection}")
    private int maxConnections;
    
    @Bean
    public AmazonS3 amazonS3() {
        ClientConfiguration awsConfiguration = new ClientConfiguration();
        awsConfiguration.setMaxConnections(maxConnections);
        AmazonS3Client s3Client = new AmazonS3Client(new BasicAWSCredentials(accessKeyId, secretKey), awsConfiguration);
        s3Client.setEndpoint(s3Endpoint);
        return s3Client;
    }
}
